# Pecha Kucha

* [Kurs als Ebook](https://mastecker.gitlab.io/pecha_kucha/course.epub)
* [Kurs als PDF](https://mastecker.gitlab.io/pecha_kucha/course.pdf)
* [Kurs als HTML](https://mastecker.gitlab.io/pecha_kucha/index.html)
